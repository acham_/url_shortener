import threading
import uuid

SHORT_URL_PREFIX = "https://short.co/"

short_to_long_map = {}
long_to_short_map = {}
lock = threading.Lock()


def create_add_short_key(long_url):
    """Given a long URL, attempts to create a random short key and add it to the DB;
    repeats until a new short key is generated.
    Note: single-threaded, since a call to this function is enclosed in a lock."""

    short_key = uuid.uuid4().hex[:6]
    long_found = short_to_long_map.get(short_key)
    while long_found is not None:
        short_key = uuid.uuid4().hex[:6]
        long_found = short_to_long_map.get(short_key)

    # We have a unique short key: add it to the map and release the lock
    short_to_long_map[short_key] = long_url

    return short_key


def get_short_url(long_url):
    """Given a long URL, either returns an existing corresponding short URL,
    or creates one and returns it."""

    lock.acquire()
    short_key = long_to_short_map.get(long_url)
    if short_key is not None:
        lock.release()
        return f"{SHORT_URL_PREFIX}{short_key}"

    # Short URL does not exist: create and add it
    short_key = create_add_short_key(long_url)
    long_to_short_map[long_url] = short_key
    lock.release()

    return f"{SHORT_URL_PREFIX}{short_key}"


def get_long_url(short_url):
    """Given a short url, returns the corresponding long URL if it exists, and None if it doesn't."""

    # Error checking
    if short_url is None \
            or len(short_url) <= len(SHORT_URL_PREFIX) \
            or short_url[:len(SHORT_URL_PREFIX)] != SHORT_URL_PREFIX:
        return None

    short_key = short_url[len(SHORT_URL_PREFIX):]
    long_found = short_to_long_map.get(short_key)

    return long_found
