# URL Shortener
A web service that:

 - takes a full URL and returns a corresponding short URL
 - takes a shortened URL and returns the corresponding full URL

This uses the Flask framework with multi-threading enabled.

Short URLs are created from random UUIDs so as to generate private short URLs (ie. the long form is not retrievable from the short form).

To achieve the bi-directional mapping between short and long URLs, two dictionaries are used along with
 a lock to control access by concurrent requests.
 The reason for using two dictionaries (and therefore having duplicate data) is that this allows
 for fast lookup in either direction. The lock allows for concurrent readers
 in the short-to-long URL direction, but for a single reader/writer at a time in 
 the long-to-short direction.

The request handling layer (`main.py`) is separated from the data management layer (`db.py`).

Time taken: about 3 hours.

## Running
Server, from a `bash` shell:
 
 - Create a virtualenv:     `$ python3 -m venv venv`
 - Activate virtualenv:     `$ source ./venv/bin/activate`
 - Install requirements:    `$ pip3 install -r requirements.txt`
 - Launch service:          `$ python3 main.py`
 
Client:

 - Long URL to Short: `GET or POST localhost:5000/shorten&url=<long_url>`
 - Short URL to Long: `GET localhost:5000/lengthen&url=<short_url>` 
 
 Note: long URL must be a valid URL, including a prefix.

## Limitations & Next Steps
Since the data (mappings between short and long URLs) is currently stored in the service process itself, if the service crashes, the data is lost.
A key improvement would be to separate the data from the service, for example storing 
mappings between short and long URLs as objects in a MongoDB instance, which offers fast lookups
as well as persistence.

With more time, I would look into whether the Flask `threaded=True` option allows
for true concurrency, or whether execution is still limited to one thread at 
a time due to Python's Global Interpreter Lock, in which case the lock usage
can be more coarse-grained and a different approach can be taken to allow
for concurrent reads.

For simplicity, URLs are exchanged through query parameters, which is a little awkward
since this leads to a "URL within a URL" and goes against the principle
 that query parameters should mainly be used for filtering resources. 
 On a real project, I would put these in the request body.
