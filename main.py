import logging as LOG
import validators
from flask import Flask, request, make_response, Response

import db

# Log configuration
LOG.basicConfig(format='[APP] %(asctime)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S',
                    level=LOG.INFO)

flask_app = Flask(__name__)


@flask_app.route('/health', methods=['GET'])
def health():
    """Service health check."""

    return "URL Shortener service is up."


@flask_app.route('/shorten', methods=['GET', 'POST'])
def shorten():
    """Endpoint to get a short URL from a long one, creating one if necessary."""

    long_url = request.args.get('url', default=None, type=str)

    # Error checking: long URL must be a valid URL
    if long_url is None or not validators.url(long_url):
        return make_response('Missing or bad input URL', 400)

    short_url = db.get_short_url(long_url)
    return short_url


@flask_app.route('/lengthen', methods=['GET'])
def lengthen():
    """Endpoint to get a long URL from its corresponding short URL, returning a 404 if not found."""

    short_url = request.args.get('url', default=None, type=str)

    if short_url is None or not validators.url(short_url):
        return Response('Missing or bad input URL', status=400, mimetype='text/plain')

    long_url = db.get_long_url(short_url)
    if long_url is None:
        return make_response("Not found", 404)

    return long_url


if __name__ == '__main__':
    LOG.info("URL Shortener Service start")
    flask_app.run(threaded=True)
